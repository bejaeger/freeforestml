Examples
========

.. toctree::
   :maxdepth: 2
   :hidden:

   ToyData.ipynb
   Histogram.ipynb
   HistogramFactory.ipynb
   Blinding.ipynb
   UHepp.ipynb
   SystematicsBand.ipynb
   ConfusionMatrix.ipynb
   Correlation.ipynb
   ROC.ipynb
   Classification.ipynb
   TmvaBdt.ipynb
