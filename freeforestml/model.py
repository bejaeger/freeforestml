from abc import ABC, abstractmethod

import os
import sys
import h5py
import json

import numpy as np
import pandas as pd

import tensorflow

from freeforestml.variable import Variable
from freeforestml.helpers import python_to_str, str_to_python


class CrossValidator(ABC):
    """
    Abstract class of a cross validation method.
    """

    def __init__(self, k, mod_var=None, frac_var=None):
        """
        Creates a new cross validator. The argument k determines the number of
        folders.  The mod_var specifies a variable whose 'mod k' value defines
        the set. The frac_var specifies a variable whose decimals defines the
        set. Only one of the two can be used. Both options can be either a
        string naming the column in the dataframe or a variable object.
        """
        self.k = k

        if (mod_var is None) == (frac_var is None):
            raise TypeError("Exactly one of mod_var or frac_var must be "
                            "used.")
        elif mod_var is not None:
            self.variable = mod_var
            self.mod_mode = True
        else:
            self.variable = frac_var
            self.mod_mode = False

        # Handle variable
        if isinstance(self.variable, str):
            self.variable = Variable(self.variable, self.variable)

    def __eq__(self, other):
        """
        Compare if two cross validators are the same.
        """
        if not isinstance(other, self.__class__):
            return False

        if self.k != other.k:
            return False

        if self.mod_mode != other.mod_mode:
            return False

        if self.variable != other.variable:
            return False

        return True

    @abstractmethod
    def select_slice(self, df, slice_id):
        """
        Returns the index array to select all events from the dataset of a
        given slice.

        NB: This method is for internal usage only. There might be more than k
        slices.
        """

    @abstractmethod
    def select_training(self, df, fold_i, for_predicting=False):
        """
        Returns the index array to select all training events from the dataset for the
        given fold.
        """

    @abstractmethod
    def select_validation(self, df, fold_i):
        """
        Returns the index array to select all validation events from the dataset for the
        given fold.
        """

    @abstractmethod
    def select_test(self, df, fold_i):
        """
        Returns the index array to select all test events from the dataset for the
        given fold.
        """

    def select_cv_set(self, df, cv, fold_i, for_predicting=False):
        """
        Returns the index array to select all events from the cross validator
        set specified with cv ('train', 'val', 'test') for the given fold.
        """
        if cv not in ['train', 'val', 'test']:
            raise ValueError("Argument 'cv' must be one of 'train', 'val', "
                             "'test', 'all'; but was %s." % repr(cv))
        if cv == "train":
            selected = self.select_training(
                df, fold_i, for_predicting=for_predicting)
        elif cv == "val":
            selected = self.select_validation(df, fold_i)
        else:
            selected = self.select_test(df, fold_i)
        return selected

    def retrieve_fold_info(self, df, cv):
        """
        Returns and array of integers to specify which event was used
        for train/val/test in which fold. Mostly useful for the inference/predict
        step. For cross validators with a high number of folds, so that an event
        is used in multiple folds for the training set, a single fold number is
        retrieved so that the folds are equally represented in the predicted
        training data.
        """
        fold_info = np.zeros(len(df), dtype='bool') - 1
        for fold_i in range(self.k):
            selected = self.select_cv_set(df, cv, fold_i, True)
            fold_info[selected] = fold_i
        return fold_info

    def save_to_h5(self, path, key, overwrite=False):
        """
        Save cross validator definition to a hdf5 file.
        'path' is the file path and 'key' is the path inside the hdf5 file.
        If overwrite is true then already existing file contents are overwritten.
        """
        if overwrite:
            open_mode = "w"
        else:
            open_mode = "a"
        with h5py.File(path, open_mode) as output_file:
            group = output_file.create_group(os.path.join(key))
            group.attrs["class_name"] = np.string_(self.__class__.__name__)
            group.attrs["k"] = self.k
            group.attrs["mod_mode"] = self.mod_mode
        self.variable.save_to_h5(path, os.path.join(key, "variable"))

    @classmethod
    def load_from_h5(cls, path, key):
        """
        Create a new cross validator instance from an hdf5 file.
        'path' is the file path and 'key' is the path inside the hdf5 file.
        """
        with h5py.File(path, "r") as input_file:
            class_name = input_file[key].attrs["class_name"].decode()
            class_object = getattr(sys.modules[__name__], class_name)
            k = input_file[key].attrs["k"]
            mod_mode = input_file[key].attrs["mod_mode"]
            variable = Variable.load_from_h5(
                path, os.path.join(key, "variable"))
            if mod_mode:
                return class_object(k=k, mod_var=variable)
            else:
                return class_object(k=k, frac_var=variable)


class ClassicalCV(CrossValidator):
    """
    Performs the k-fold cross validation on half of the data set. The other
    half is designated as the test set.

        fold 0: | Tr | Tr  | Tr | Tr | Va |          Test          |
        fold 1: | Tr | Tr  | Tr | Va | Tr |          Test          |
        fold 2: | Tr | Tr  | Va | Tr | Tr |          Test          |
        fold 3: | Tr | Va  | Tr | Tr | Tr |          Test          |
        fold 4: | Va | Tr  | Tr | Tr | Tr |          Test          |

        Va=Validation, Tr=Training
    """

    def select_slice(self, df, slice_id):
        """
        Returns the index array to select all events from the dataset of a
        given slice.

        NB: This method is for internal usage only. There might be more than k
        slices.
        """
        if self.mod_mode:
            return (self.variable(df) % (self.k * 2) == slice_id)
        else:
            variable = self.variable(df) % 1
            return (slice_id / (self.k * 2.0) <= variable) \
                & (variable < (slice_id + 1.0) / (self.k * 2))

    def select_training(self, df, fold_i, for_predicting=False):
        """
        Returns the index array to select all training events from the dataset for the
        given fold.
        """
        selected = np.zeros(len(df), dtype='bool')
        for slice_i in range(self.k):
            if (slice_i + fold_i) % self.k == self.k - 1:
                continue

            selected = selected | self.select_slice(df, slice_i)
        return selected

    def select_validation(self, df, fold_i):
        """
        Returns the index array to select all validation events from the dataset for the
        given fold.
        """
        return self.select_slice(df, (self.k - fold_i - 1) % self.k)

    def select_test(self, df, fold_i):
        """
        Returns the index array to select all test events from the dataset for the
        given fold.
        """
        selected = np.zeros(len(df), dtype='bool')
        for slice_i in range(self.k, self.k * 2):
            selected = selected | self.select_slice(df, slice_i)

        return selected


class NoTestCV(CrossValidator):
    """
    Uses the whole dataset for training and validation with a single fold. The
    test set is empty.

        fold 0: |              Training               |    Val    |

    The NoTestCV can be useful if the test dataset is provided independently
    from the training and validation, for example if a different generator is
    used for the training or if real-time (non-hep) data is used as a "test"
    set.
    """

    def __init__(self, mod_var=None, frac_var=None, k=10):
        """
        The parameter k defines the inverse fraction of the validation set.
        For example, k=5 will allocate 1/5 = 20% of the dataset for validation.
        """
        super().__init__(k, mod_var=mod_var, frac_var=frac_var)

    def select_slice(self, df, slice_id):
        """
        Returns the index array to select all events from the dataset of a
        given slice.

        NB: This method is for internal usage only. There might be more than k
        slices.
        """
        if self.mod_mode:
            return (self.variable(df) % self.k == slice_id)
        else:
            variable = self.variable(df) % 1
            return (slice_id / self.k <= variable) \
                & (variable < (slice_id + 1.0) / self.k)

    def select_training(self, df, fold_i, for_predicting=False):
        """
        Returns the index array to select all training events from the
        dataset. The fold_i parameter has no effect.
        """
        selected = np.zeros(len(df), dtype='bool')
        for slice_i in range(1, self.k):
            selected = selected | self.select_slice(df, slice_i)

        return selected

    def select_validation(self, df, fold_i):
        """
        Returns the index array to select all validation events from the dataset for the
        given fold.
        """
        return self.select_slice(df, 0)

    def select_test(self, df, fold_i):
        """
        Returns the index array to select all test events from the dataset for the
        given fold. The test set is empty.
        """
        selected = np.zeros(len(df), dtype='bool')
        return selected


class BinaryCV(CrossValidator):
    """
    Defines a training set and a test set using a binary split. There is no
    independent validation set in this case. The BinaryCV should not be used
    for parameter optimization.

        fold 0: |      Training      |       Test & Val       |
        fold 1: |     Test & Val     |        Training        |

    The BinaryCV can be used after parameter optimization with ClassicalCV to
    retrain the model on the full half. The valiation performance contain in
    HepNet.history is the test performance.
    """

    def __init__(self, mod_var=None, frac_var=None, k=None):
        """
        k is set to 2. The argument k has no effect.
        """
        super().__init__(2, mod_var=mod_var, frac_var=frac_var)

    def select_slice(self, df, slice_id):
        """
        Returns the index array to select all events from the dataset of a
        given slice.

        NB: This method is for internal usage only. There might be more than k
        slices.
        """
        if self.mod_mode:
            return (self.variable(df) % self.k == slice_id)
        else:
            variable = self.variable(df) % 1
            return (slice_id / self.k <= variable) \
                & (variable < (slice_id + 1.0) / self.k)

    def select_training(self, df, fold_i, for_predicting=False):
        """
        Returns the index array to select all training events from the dataset for the
        given fold.
        """
        return self.select_slice(df, fold_i)

    def select_validation(self, df, fold_i):
        """
        Returns the index array to select all validation events from the dataset for the
        given fold.
        """
        return self.select_slice(df, (1 + fold_i) % self.k)

    def select_test(self, df, fold_i):
        """
        Returns the index array to select all test events from the dataset for the
        given fold.
        """
        return self.select_slice(df, (1 + fold_i) % self.k)


class MixedCV(CrossValidator):
    """
    Performs the k-fold cross validation where validation and test sets are
    both interleaved.

        fold 0: | Tr | Tr  | Tr | Te | Va |
        fold 1: | Tr | Tr  | Te | Va | Tr |
        fold 2: | Tr | Te  | Va | Tr | Tr |
        fold 3: | Te | Va  | Tr | Tr | Tr |
        fold 4: | Va | Tr  | Tr | Tr | Te |

        Va=Validation, Tr=Training, Te=Test
    """

    def select_slice(self, df, slice_id):
        """
        Returns the index array to select all events from the dataset of a
        given slice.

        NB: This method is for internal usage only. There might be more than k
        slices.
        """
        if self.mod_mode:
            return (self.variable(df) % self.k == slice_id)
        else:
            variable = self.variable(df) % 1
            return (slice_id / self.k <= variable) \
                & (variable < (slice_id + 1.0) / self.k)

    def select_training_slices(self, fold_i, for_predicting=False):
        """
        Returns array with integers corresponding
        to the data slices used in training fold_i.
        If 'for_predicting' is set to True only one slice
        is returned for each fold so that the folds are equally represented
        in the predicted training data.
        """
        all_slices_for_folds = []
        for fold in range(self.k):
            all_slices_for_folds.append([])
            for slice_i in range(self.k):
                if (slice_i + fold) % self.k == self.k - 1:
                    continue
                if (slice_i + fold) % self.k == self.k - 2:
                    continue
                all_slices_for_folds[-1].append(slice_i)

        # if we select the slices for training we are done
        if not for_predicting:
            return all_slices_for_folds[fold_i]

        # all_slices_for_folds looks e.g. like:
        # [[0, 1, 2], [0, 1, 4], [0, 3, 4], [2, 3, 4], [1, 2, 3]]
        # need to select array with uniq entries:
        # [0, 1, 2, 4, 3]
        def uniq_el(ar): return set(x for l in ar for x in l)
        exclusive_slices = []
        for i, slices in enumerate(all_slices_for_folds):
            for sl in slices:
                if sl not in exclusive_slices and sl in uniq_el(all_slices_for_folds[i:]):
                    exclusive_slices.append(sl)
        return [exclusive_slices[fold_i]]

    def select_training(self, df, fold_i, for_predicting=False):
        """
        Returns the index array to select all training events from the dataset for the
        given fold.
        """
        selected = np.zeros(len(df), dtype='bool')
        slices = self.select_training_slices(
            fold_i, for_predicting=for_predicting)
        for slice_i in slices:
            selected = selected | self.select_slice(df, slice_i)

        return selected

    def select_validation(self, df, fold_i):
        """
        Returns the index array to select all validation events from the dataset for the
        given fold.
        """
        return self.select_slice(df, (self.k - fold_i - 1) % self.k)

    def select_test(self, df, fold_i):
        """
        Returns the index array to select all test events from the dataset for the
        given fold.
        """
        return self.select_slice(df, (self.k - fold_i - 2) % self.k)


class Normalizer(ABC):
    """
    Abstract normalizer which shift and scales the distribution such that it hash
    zero mean and unit width.
    """

    @abstractmethod
    def __init__(self, df, input_list=None):
        """
        Returns a normalizer object with the normalization moments stored
        internally. The input_list argument specifies which inputs should be
        normalized. All other columns are left untouched.
        """

    @abstractmethod
    def __call__(self, df):
        """
        Applies the normalized of the input_columns to the given dataframe and
        returns a normalized copy.
        """
    @abstractmethod
    def __eq__(self, other):
        """
        Check if two normalizers are the same.
        """

    @property
    @abstractmethod
    def scales(self):
        """
        Every normalizor must reduce to a simple (offset + scale * x)
        normalization to be used with lwtnn. This property returns the scale
        parameters for all variables.
        """

    @property
    @abstractmethod
    def offsets(self):
        """
        Every normalizor must reduce to a simple (offset + scale * x)
        normalization to be used with lwtnn. This property returns the offset
        parameters for all variables.
        """

    def save_to_h5(self, path, key, overwrite=False):
        """
        Save normalizer definition to a hdf5 file.
        'path' is the file path and 'key' is the path inside the hdf5 file.
        If overwrite is true then already existing file contents are overwritten.
        """
        if overwrite:
            open_mode = "w"
        else:
            open_mode = "a"
        with h5py.File(path, open_mode) as output_file:
            group = output_file.create_group(os.path.join(key))
            group.attrs["class_name"] = np.string_(self.__class__.__name__)
        self._save_to_h5(path, key)

    @abstractmethod
    def _save_to_h5(self, path, key):
        """
        Save child class specific definitions to a hdf5 file.
        'path' is the file path and 'key' is the path inside the hdf5 file.
        If overwrite is true then already existing file contents are overwritten.
        """

    @classmethod
    def load_from_h5(cls, path, key):
        """
        Create a new normalizer instance from an hdf5 file.
        'path' is the file path and 'key' is the path inside the hdf5 file.
        """
        with h5py.File(path, "r") as input_file:
            if key not in input_file:
                return None
            class_name = input_file[key].attrs["class_name"].decode()
            class_object = getattr(sys.modules[__name__], class_name)
        return class_object._load_from_h5(path, key)

    @classmethod
    @abstractmethod
    def _load_from_h5(cls, path, key):
        """
        Load child class specific definitions from a hdf5 file.
        """


class EstimatorNormalizer(Normalizer):
    """
    Normalizer which uses estimators to compute the normalization moments.
    This method might be lead to sub-optimal results if there are outliers.
    """

    def __init__(self, df, input_list=None, center=None, width=None):
        """
        See base class.
        """
        if center is not None and width is not None:
            self.center = center
            self.width = width
        else:
            if input_list is not None:
                df = df[input_list]

            self.center = df.mean()
            self.width = df.std()

            self.width[self.width == 0] = 1

    def __call__(self, df):
        """
        See base class.
        """
        input_list = list(self.center.index)
        normed = (df[input_list] - self.center) / self.width

        aux_list = [c for c in df.columns if c not in input_list]
        normed[aux_list] = df[aux_list]
        return normed

    def __eq__(self, other):
        """
        See base class.
        """
        if not isinstance(other, self.__class__):
            return False

        if not self.center.equals(other.center):
            return False

        if not self.width.equals(other.width):
            return False

        return True

    def _save_to_h5(self, path, key):
        """
        See base class.
        """
        self.center.to_hdf(path, key=os.path.join(key, "center"))
        self.width.to_hdf(path, key=os.path.join(key, "width"))

    @classmethod
    def _load_from_h5(cls, path, key):
        """
        See base class.
        """
        center = pd.read_hdf(path, os.path.join(key, "center"))
        width = pd.read_hdf(path, os.path.join(key, "width"))
        return cls(None, center=center, width=width)

    @property
    def scales(self):
        return 1 / self.width

    @property
    def offsets(self):
        return -self.center / self. width


def normalize_category_weights(df, categories, weight='weight'):
    """
    The categorical weight normalizer acts on the weight variable only. The
    returned dataframe will satisfy the following conditions:
      - The sum of weights of all events is equal to the total number of
        entries.
      - The sum of weights of a category is equal to the total number of entries
        divided by the number of classes. Therefore the sum of weights of two
        categories are equal.
      - The relative weights within a category are unchanged.
    """

    df_out = df[:]
    w_norm = np.empty(len(df))
    for category in categories:
        idx = category.idx_array(df)
        w_norm[idx] = df[idx][weight].sum()

    df_out[weight] = df_out[weight] / w_norm * len(df) / len(categories)

    return df_out


class HepNet:
    """
    Meta model of a concrete neural network around the underlying Keras model.
    The HEP net handles cross validation, normalization of the input
    variables, the input weights, and the actual Keras model. A HEP net has no
    free parameters.
    """

    def __init__(self, keras_model, cross_validator, normalizer, input_list,
                 output_list, wandb_log_func=None):
        """
        Creates a new HEP model. The keras model parameter must be a class that
        returns a new instance of the compiled model (The HEP net needs to
        able to create multiple models, one for each cross validation fold.)

        The cross_validator must be a CrossValidator object.

        The normalizer must be a Normalizer class that returns a normalizer. Each
        cross_validation fold uses a separate normalizer with independent
        normalization weights.

        The input and output lists are lists of variables of column names used
        as input and target of the keras model. The input is normalized.
        """
        self.model_cls = keras_model
        self.cv = cross_validator
        self.norm_cls = normalizer
        self.input_list = input_list
        self.output_list = output_list
        self.wandb_log_func = wandb_log_func
        self.norms = []
        self.models = []
        self.history = pd.DataFrame()

    def __eq__(self, other):
        """
        Check if two models have the same configuration.
        """
        if not isinstance(other, self.__class__):
            return False

        if python_to_str(self.model_cls) != python_to_str(other.model_cls):
            return False

        if self.cv != other.cv:
            return False

        if python_to_str(self.norm_cls) != python_to_str(other.norm_cls):
            return False

        if self.input_list != other.input_list:
            return False

        if self.output_list != other.output_list:
            return False

        if self.norms != other.norms:
            return False

        if (self.history != other.history).all().all():
            return False

        return True

    def fit(self, df, train_weight=None, event_weight=None, all_callbacks=None, **kwds):
        """
        Calls fit() on all folds. All kwds are passed to fit().
        """
        # For the weights we have event weights and sample weights.
        # We want to distinguish between both to use custom callbacks
        if train_weight is None:
            train_weight = Variable("unity", lambda d: np.ones(len(d)))
        elif isinstance(train_weight, str):
            train_weight = Variable(train_weight, train_weight)
        if event_weight is None:
            event_weight = Variable("unity", lambda d: np.ones(len(d)))
        elif isinstance(event_weight, str):
            event_weight = Variable(event_weight, event_weight)

        # Loop over folds:
        self.norms = []
        self.models = []
        self.history = pd.DataFrame()

        for fold_i in range(self.cv.k):
            # select training set
            selected = self.cv.select_training(df, fold_i)
            training_df = df[selected]

            # select validation set
            selected = self.cv.select_validation(df, fold_i)
            validation_df = df[selected]

            # seed normalizers
            norm = self.norm_cls(training_df, self.input_list)
            self.norms.append(norm)
            training_df = norm(training_df)
            validation_df = norm(validation_df)

            # fit folds
            model = self.model_cls()
            self.models.append(model)

            # check for and initialize custom callbacks
            lazily_initialized_callbacks = []
            lazily_initialized_callbacks_names = []
            for cc in all_callbacks:
                if isinstance(cc, dict):
                    if "Z0Callback" in cc.keys():
                        c_tmp = cc["Z0Callback"](validation_df[self.input_list], validation_df[self.output_list], np.array(
                            event_weight(validation_df)), "val")
                        lazily_initialized_callbacks.append(c_tmp)

                        c_tmp2 = cc["Z0Callback"](training_df[self.input_list], training_df[self.output_list], np.array(event_weight(training_df)), "train")
                        lazily_initialized_callbacks.append(c_tmp2)

                if cc == "Z0Callback":  # callback that retrieves significance
                    lazily_initialized_callbacks.append(Z0Callback(validation_df[self.input_list],
                                                                   validation_df[self.output_list],
                                                                   # only use event weights, no sample weights
                                                                   np.array(event_weight(validation_df)), self.wandb_log_func))
                    lazily_initialized_callbacks_names.append(cc)
                if cc == "MultiClassZ0Callback":  # callback that retrieves significance
                    lazily_initialized_callbacks.append(MultiClassZ0Callback(validation_df[self.input_list],
                                                                             validation_df[self.output_list],
                                                                             # only use event weights, no sample weights
                                                                             np.array(event_weight(validation_df)), self.wandb_log_func))
                    lazily_initialized_callbacks_names.append(cc)
            callbacks = [
                c for c in all_callbacks if not c in lazily_initialized_callbacks_names and not isinstance(c, dict)] + lazily_initialized_callbacks

            history = model.fit(training_df[self.input_list],
                                training_df[self.output_list],
                                validation_data=(
                                    validation_df[self.input_list],
                                    validation_df[self.output_list],
                                    np.array(train_weight(validation_df)),
            ),
                sample_weight=np.array(
                                    train_weight(training_df)),
                callbacks=callbacks,
                **kwds)

            history = history.history
            history['fold'] = np.ones(
                len(history['loss']), dtype='int') * fold_i
            history['epoch'] = np.arange(len(history['loss']))
            self.history = pd.concat([self.history, pd.DataFrame(history)])

    def predict(self, df, cv='val', retrieve_fold_info=False, **kwds):
        """
        Calls predict() on the Keras model. The argument cv specifies the
        cross validation set to select: 'train', 'val', 'test'.
        Default is 'val'.

        All other keywords are passed to predict.
        """
        if cv not in ['train', 'val', 'test']:
            raise ValueError("Argument 'cv' must be one of 'train', 'val', "
                             "'test', 'all'; but was %s." % repr(cv))

        out = np.zeros((len(df), len(self.output_list)))
        test_set = np.zeros(len(df), dtype='bool')

        for fold_i in range(self.cv.k):
            model = self.models[fold_i]
            norm = self.norms[fold_i]

            # identify fold
            selected = self.cv.select_cv_set(
                df, cv, fold_i, for_predicting=True)

            test_set |= selected
            out[selected] = model.predict(norm(df[selected][self.input_list]),
                                          **kwds)

        test_df = df[test_set]
        out = out[test_set].transpose()
        out = dict(zip(["pred_" + s for s in self.output_list], out))
        test_df = test_df.assign(**out)

        if retrieve_fold_info:
            fold = {cv + "_fold":  self.cv.retrieve_fold_info(df, cv)}
            test_df = test_df.assign(**fold)

        return test_df

    def save(self, path):
        """
        Save the model and all associated components to a hdf5 file.
        """

        # save model architecture and weights (only if already trained)
        if len(self.models) == self.cv.k:
            for fold_i in range(self.cv.k):
                path_token = path.rsplit(".", 1)
                if len(path_token) == 1:
                    path_token.append(f"fold_{fold_i}")
                else:
                    path_token.insert(-1, f"fold_{fold_i}")

                # this is the built-in save function from keras
                self.models[fold_i].save(".".join(path_token))

        with h5py.File(path, "w") as output_file:
            # save default model class
            # since this is a arbitrary piece of python code we need to use the python_to_str function
            # The following is not working with tensorflow 2.0 and disabled eager_execution
            # the following error is thrown:
            # NotImplementedError: numpy() is only available when eager execution is enabled.
            group = output_file.create_group("models/default")
            group.attrs["model_cls"] = np.string_(
                python_to_str(self.model_cls))

            # save class name of default normalizer as string
            group = output_file.create_group("normalizers/default")
            group.attrs["norm_cls"] = np.string_(self.norm_cls.__name__)

        # save cross_validator
        self.cv.save_to_h5(path, "cross_validator")

        # save normalizer (only if already trained)
        if len(self.norms) == self.cv.k:
            for fold_i in range(self.cv.k):
                self.norms[fold_i].save_to_h5(
                    path, "normalizers/fold_{}".format(fold_i))

        # save input/output lists
        pd.DataFrame(self.input_list).to_hdf(path, "input_list")
        pd.DataFrame(self.output_list).to_hdf(path, "output_list")

        # save training history
        self.history.to_hdf(path, "history")

    @classmethod
    def load(cls, path):
        """
        Restore a model from a hdf5 file.
        """
        # load default model and normalizer
        with h5py.File(path, "r") as input_file:
            model = str_to_python(
                input_file["models/default"].attrs["model_cls"].decode())
            normalizer_class_name = input_file["normalizers/default"].attrs["norm_cls"].decode()
            normalizer = getattr(sys.modules[__name__], normalizer_class_name)

        # load cross validator
        cv = CrossValidator.load_from_h5(path, "cross_validator")

        # load input/output lists
        input_list = list(pd.read_hdf(path, "input_list")[0])
        output_list = list(pd.read_hdf(path, "output_list")[0])

        # create instance
        instance = cls(model, cv, normalizer, input_list, output_list)

        # load history
        history = pd.read_hdf(path, "history")
        instance.history = history

        # load trained models (if existing)
        with h5py.File(path, "r") as input_file:
            for fold_i in range(cv.k):
                path_token = path.rsplit(".", 1)
                if len(path_token) == 1:
                    path_token.append(f"fold_{fold_i}")
                else:
                    path_token.insert(-1, f"fold_{fold_i}")

                model = tensorflow.keras.models.load_model(
                    ".".join(path_token))
                instance.models.append(model)

        # load normalizer
        for fold_i in range(cv.k):
            norm = Normalizer.load_from_h5(
                path, "normalizers/fold_{}".format(fold_i))
            if norm is not None:
                instance.norms.append(norm)

        return instance

    def export(self, path_base, command="converters/keras2json.py",
               expression={}):
        """
        Exports the network such that it can be converted to lwtnn's json
        format. The method generate a set of files for each cross validation
        fold. For every fold, the archtecture, the weights, the input
        variables and their normalization is exported. To simplify the
        conversion to lwtnn's json format, the method also creates a bash
        script which converts all folds.

        The path_base argument should be a path or a name of the network. The
        names of the generated files are created by appending to path_base.

                The optional expression can be used to inject the CAF expression when
        the NN is used. The final json file will contain an entry KEY=VALUE if
        a variable matches the dict key.
        """
        for fold_i in range(self.cv.k):
            # get the architecture as a json string
            arch = self.models[fold_i].to_json()
            # save the architecture string to a file somehow, the below will work
            with open('%s_arch_%d.json' % (path_base, fold_i), 'w') as arch_file:
                arch_file.write(arch)

            # now save the weights as an HDF5 file
            self.models[fold_i].save_weights(
                '%s_wght_%d.h5' % (path_base, fold_i))

            with open("%s_vars_%d.json" % (path_base, fold_i), "w") \
                    as variable_file:
                scales = self.norms[fold_i].scales
                offsets = self.norms[fold_i].offsets
                offsets = [o / s for o, s in zip(offsets, scales)]

                variables = [("%s=%s" % (v, expression[v]))
                             if v in expression else v
                             for v in self.input_list]

                inputs = [dict(name=v, offset=o, scale=s)
                          for v, o, s in zip(variables, offsets, scales)]

                json.dump(dict(inputs=inputs, class_labels=self.output_list),
                          variable_file)

            mode = "w" if fold_i == 0 else "a"
            with open("%s.sh" % path_base, mode) as script_file:
                print(f"{command} {path_base}_arch_{fold_i}.json "
                      f"{path_base}_vars_{fold_i}.json "
                      f"{path_base}_wght_{fold_i}.h5 "
                      f"> {path_base}_{fold_i}.json", file=script_file)


class Z0Callback(tensorflow.keras.callbacks.Callback):

    def __init__(self, X_valid=0, Y_valid=0, W_valid=0, wandb_log=None):
        self.X_valid = np.array(X_valid)
        self.Y_valid = np.array(Y_valid)
        self.W_valid = np.array(W_valid)
        self.W_valid = self.W_valid.reshape((self.W_valid.shape[0], 1))
        self.wandb_log = wandb_log

    def add_to_history(self, Z0):
        if "Z0" in self.model.history.history.keys():
            self.model.history.history["Z0"].append(Z0)
        else:  # first epoch
            self.model.history.history["Z0"] = [Z0]
        if not self.wandb_log is None:
            self.wandb_log({"Z0": Z0})

    def on_epoch_end(self, epoch, logs=None):

        y_pred = np.array(self.model.predict(self.X_valid, batch_size=4096))
        w_bkg = self.W_valid[self.Y_valid == 0]
        w_sig = self.W_valid[self.Y_valid == 1]
        y_bkg = y_pred[self.Y_valid == 0]
        y_sig = y_pred[self.Y_valid == 1]

        c_sig, edges = np.histogram(y_sig, 20, weights=w_sig, range=(0, 1))
        c_bkg, edges = np.histogram(y_bkg, 20, weights=w_bkg, range=(0, 1))

        def Z0_func(s, b): return np.sqrt(2*((s+b) * np.log1p(s/b) - s))
        z_list = [Z0_func(si, bi)
                  for si, bi in zip(c_sig, c_bkg) if bi > 0 and si > 0]
        Z0 = np.sqrt(np.sum(np.square(z_list)))

        self.add_to_history(Z0)

        print("\nINFO: Significance in epoch {} is Z0 = {}".format(epoch, Z0))


class MultiClassZ0Callback(tensorflow.keras.callbacks.Callback):

    def __init__(self, X=0, Y=0, W=0, targets="", wandb_log=None, plot_hists=True):
        self.X = np.array(X)
        self.VBF_target = np.array(Y["VBF_target"])
        self.ggF_target = np.array(Y["ggF_target"])
        self.bkg_target = np.array(Y["bkg_target"])
        self.W_valid = np.array(W)
        self.W_valid = self.W_valid.reshape((self.W_valid.shape[0], 1))

        self.wandb_log = wandb_log
        self.plot_hists = plot_hists

    def add_to_history(self, key, val):
        if key in self.model.history.history.keys():
            self.model.history.history[key].append(val)
        else:  # first epoch
            self.model.history.history[key] = [val]

    def on_epoch_end(self, epoch, logs=None):
        # predict. Will output a nx3 array
        y_pred = np.array(self.model.predict(self.X, batch_size=4096))

        # have shape (n,1)
        w_VBF = self.W_valid[self.VBF_target == 1]
        w_VBF_bkg = self.W_valid[self.VBF_target == 0]

        w_ggF = self.W_valid[self.ggF_target == 1]
        w_ggF_bkg = self.W_valid[self.ggF_target == 0]

        # we want to normalize the weights to remove the dependence
        # of this metric on the size of the val set that is used

        # 260 is ~expected number of VBF events in common VBF/ggF SR
        w_VBF = w_VBF / sum(w_VBF) * 250
        # 2300 is ~expected number of total bkg events in common VBF/ggF SR
        w_VBF_bkg = w_VBF_bkg / sum(w_VBF_bkg) * 60000

        # 684 is ~expected number of ggF events in common VBF/ggF SR
        w_ggF = w_ggF / sum(w_ggF) * 500
        # 2300 is ~expected number of total bkg events in common VBF/ggF SR
        w_ggF_bkg = w_ggF_bkg / sum(w_ggF_bkg) * 60000

        # get predictions for individual process in arrays
        # The order is as provided in the config files
        # which for now is VBF, ggF, bkg
        # shape (n,)
        # VBF predictions
        y_VBF = y_pred[self.VBF_target == 1, 0]
        y_VBF_bkg = y_pred[self.VBF_target == 0, 0]
        # ggF predictions
        y_ggF = y_pred[self.ggF_target == 1, 1]
        y_ggF_bkg = y_pred[self.ggF_target == 0, 1]

        # reshape to (n, 1)
        y_VBF = y_VBF.reshape((y_VBF.shape[0], 1))
        y_VBF_bkg = y_VBF_bkg.reshape((y_VBF_bkg.shape[0], 1))
        y_ggF = y_ggF.reshape((y_ggF.shape[0], 1))
        y_ggF_bkg = y_ggF_bkg.reshape((y_ggF_bkg.shape[0], 1))

        # make histograms contents
        bins = 20
        c_VBF, edges = np.histogram(y_VBF, bins, weights=w_VBF, range=(0, 1))
        c_VBF_bkg, _ = np.histogram(
            y_VBF_bkg, bins, weights=w_VBF_bkg, range=(0, 1))
        c_ggF, _ = np.histogram(y_ggF, bins, weights=w_ggF, range=(0, 1))
        c_ggF_bkg, _ = np.histogram(
            y_ggF_bkg, bins, weights=w_ggF_bkg, range=(0, 1))

        # get significance from histograms
        Z0_VBF = self.get_Z0(c_VBF, c_VBF_bkg)
        Z0_ggF = self.get_Z0(c_ggF, c_ggF_bkg)

        print("\nINFO: Significance in epoch {} is Z0_VBF = {}, Z0_ggF = {}".format(
            epoch, Z0_VBF, Z0_ggF))

        # Add to hep net history
        self.add_to_history(key="Z0_VBF", val=Z0_VBF)
        self.add_to_history(key="Z0_ggF", val=Z0_ggF)

        if not self.wandb_log is None:
            self.wandb_log({"Z0_VBF": Z0_VBF, "Z0_ggF": Z0_ggF})

            import plotly.express as px
            import plotly.graph_objects as go
            # plotly histogram does not support weights
            histbins = 0.5 * (edges[:-1] + edges[1:])

            fig = go.Figure(layout=go.Layout(bargap=0.0, barmode="overlay", barnorm="fraction", yaxis=go.layout.YAxis(
                type="log", title="Events"), xaxis=go.layout.XAxis(title="VBF DNN output")))
            fig.add_bar(x=histbins, y=c_VBF_bkg, opacity=0.6, name="Bkg")
            fig.add_bar(x=histbins, y=c_VBF, opacity=0.6, name="Sig")
            fig.add_annotation(text="Z0 = {:.2f}".format(Z0_VBF),
                               showarrow=False, x=0.2, y=0.1)

            fig2 = go.Figure(layout=go.Layout(bargap=0.0, barmode="overlay", barnorm="fraction", yaxis=go.layout.YAxis(
                type="log", title="Events"), xaxis=go.layout.XAxis(title="ggF DNN output")))
            fig2.add_bar(x=histbins, y=c_ggF_bkg, opacity=0.6, name="Bkg")
            fig2.add_bar(x=histbins, y=c_ggF, opacity=0.6, name="Sig")
            fig2.add_annotation(text="Z0 = {:.2f}".format(
                Z0_ggF), showarrow=False, x=0.2, y=0.1)

            # also make normed plots
            c_VBF, _ = np.histogram(
                y_VBF, bins, weights=w_VBF, range=(0, 1), density=1)
            c_VBF_bkg, _ = np.histogram(
                y_VBF_bkg, bins, weights=w_VBF_bkg, range=(0, 1), density=1)
            c_ggF, _ = np.histogram(
                y_ggF, bins, weights=w_ggF, range=(0, 1), density=1)
            c_ggF_bkg, _ = np.histogram(
                y_ggF_bkg, bins, weights=w_ggF_bkg, range=(0, 1), density=1)

            fig3 = go.Figure(layout=go.Layout(bargap=0.0, barmode="overlay", barnorm="fraction", yaxis=go.layout.YAxis(
                type="log", title="Normalized Events"), xaxis=go.layout.XAxis(title="VBF DNN output")))
            fig3.add_bar(x=histbins, y=c_VBF_bkg, opacity=0.6, name="Bkg")
            fig3.add_bar(x=histbins, y=c_VBF, opacity=0.6, name="Sig")

            fig4 = go.Figure(layout=go.Layout(bargap=0.0, barmode="overlay", barnorm="fraction", yaxis=go.layout.YAxis(
                type="log", title="Normalized Events"), xaxis=go.layout.XAxis(title="ggF DNN output")))
            fig4.add_bar(x=histbins, y=c_ggF_bkg, opacity=0.6, name="Bkg")
            fig4.add_bar(x=histbins, y=c_ggF, opacity=0.6, name="Sig")

            self.wandb_log({"VBF DNN output": fig, "ggF DNN output": fig2,
                            "VBF DNN output (norm)": fig3, "ggF DNN output (norm)": fig4})

        if self.plot_hists:
            self.plot(y_VBF, w_VBF, y_VBF_bkg, w_VBF_bkg,
                      "test_plots/vbf_dnn_epoch{}.png".format(epoch), "VBF DNN output")
            self.plot(y_ggF, w_ggF, y_ggF_bkg, w_ggF_bkg,
                      "test_plots/ggF_dnn_epoch{}.png".format(epoch), "ggF DNN output")

    def get_Z0(self, h1, h2):
        z_list = [self.Z0_poisson(si, bi)
                  for si, bi in zip(h1, h2) if bi > 0 and si > 0]
        Z0 = np.sqrt(np.sum(np.square(z_list)))
        return Z0

    def Z0_poisson(self, s, b):
        return np.sqrt(2*((s+b) * np.log1p(s/b) - s))

    def plot(self, hs, ws, hb, wb, fname, xlabel, nbins=20):
        import matplotlib.pyplot as plt
        plt.rc('axes', labelsize=12)
        # puts the legend in the best possible spot in the upper right corner (0.5,0.5,0.5,0.5)
        plt.yscale("log")

        plt.hist(hs, nbins,
                 facecolor='red', alpha=1,
                 color='red',
                 range=(0, 1), density=1,
                 weights=ws,
                 histtype='step',  # bar or step
                 )
        plt.hist(hb, nbins,
                 facecolor='blue', alpha=1,
                 color='blue',
                 range=(0, 1), density=1,
                 weights=wb,
                 histtype='step',  # bar or step
                 )

        ax = plt.gca()
        ax.set_xlabel(xlabel, loc='right')
        # saves the figure at the outfilepath with the outfileName. dpi means dots per inch, essentially the resolution of the image

        plt.savefig(fname, dpi=360)
        # plt.savefig(fname, dpi=360)
        plt.clf()
